<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/about', [App\Http\Controllers\HomeController::class, 'about']);
Route::get('/services', [App\Http\Controllers\HomeController::class, 'services']);
Route::get('/service_detail/{hash}', [App\Http\Controllers\HomeController::class, 'service_detail']);
Route::get('/projects', [App\Http\Controllers\HomeController::class, 'projects']);
Route::get('/project_detail/{hash}', [App\Http\Controllers\HomeController::class, 'project_detail']);
Route::get('/clients', [App\Http\Controllers\HomeController::class, 'clients']);
Route::get('/sustainability', [App\Http\Controllers\HomeController::class, 'sustainability']);
Route::get('/careers', [App\Http\Controllers\HomeController::class, 'careers']);
Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
