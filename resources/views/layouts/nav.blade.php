<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item @yield('home')"><a href="{{ url('/') }}" class="nav-link">Home</a></li>
                <li class="nav-item @yield('about')"><a href="{{ url('/about') }}" class="nav-link">About Us</a></li>
                <li class="nav-item @yield('services')"><a href="{{ url('/services') }}" class="nav-link">Services</a></li>
                <li class="nav-item @yield('projects')"><a href="{{ url('/projects') }}" class="nav-link">Our Work</a></li>
                <li class="nav-item @yield('clients')"><a href="{{ url('/clients') }}" class="nav-link">Clients</a></li>
                <li class="nav-item @yield('sustainability')"><a href="{{ url('/sustainability') }}" class="nav-link">Sustainability</a></li>
                <li class="nav-item @yield('careers')"><a href="{{ url('/careers') }}" class="nav-link">Careers</a></li>
                <li class="nav-item @yield('contact')"><a href="{{ url('/contact') }}" class="nav-link">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>