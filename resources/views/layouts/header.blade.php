<div class="wrap">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-3 d-flex align-items-center">
                <a class="navbar-brand d-flex" href="{{ url('/') }}"><div class="icon d-flex align-items-center justify-content-center">
                    <img src="{{ asset('images/logo.png') }}" >
                </div></a>
            </div>
            <div class="col-3 d-flex justify-content-end align-items-center">
                <div class="social-media">
                    <p class="mb-0 d-flex">
                        <a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
                        <a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
                        <a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>