
<footer class="footer ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-6 col-lg">
                <div class="ftco-footer-widget mb-4">
                    <div class="col-3 d-flex align-items-center">
                        <a class="navbar-brand d-flex" href="{{ url('/') }}"><div class="icon d-flex align-items-center justify-content-center">
                            <img src="{{ asset('images/logo.png') }}" >
                        </div></a>
                    </div>
                    <p>Majeed Sons was established in 1997, in responses to the increasing global demand for Quality Engineering.</p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-4">
                        <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h2 class="ftco-heading-2">Services</h2>
                    <ul class="list-unstyled">
                       @foreach(services() as $title => $service)
                       <li><a href="{{ url('/service_detail/'.$service['slug']) }}" class="py-1 d-block"><span class="fa fa-check mr-3"></span>{{$title}}</a></li>
                       @endforeach
                   </ul>
               </div>
           </div>
           <div class="col-md-6 col-lg">
            <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Business Hours</h2>
                <div class="opening-hours">
                    <h4>Opening Days:</h4>
                    <p class="pl-3">
                        <span>Monday – Friday : 9am to 20 pm</span>
                        <span>Saturday : 9am to 17 pm</span>
                    </p>
                    <h4>Vacations:</h4>
                    <p class="pl-3">
                        <span>All Sunday Days</span>
                        <span>All Official Holidays</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg">
            <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Contact information</h2>
                <div class="block-23 mb-3">
                    <ul>
                        <li><span class="icon fa fa-map-marker"></span><span class="text">Flat # 8, 1st Floor, 2 Civic Center,
                            Moon Market, Faisal Town,
                        Lahore, Pakistan.</span></li>
                        <li><a href="#"><span class="icon fa fa-phone"></span><span class="text">+92-42-35184203</span></a></li>
                        <li><a href="#"><span class="icon fa fa-paper-plane"></span><span class="text">info@msonseng.com</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> - Majeed Sons All rights reserved.</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
