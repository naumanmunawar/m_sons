@extends('layouts.app')
@section($active_tab,"active")
@section('title', ucWords($active_tab))

@section('content')

@section('css')

<style>
    .tabulation .tab-content ul li span.fa {
        font-size: 20px !important;
    }

    .work {
        height: 350px;
    }
    .work h2 a {
        font-weight: bold;
    }

</style>
@stop

@include('layouts.header')
<section class="menu-wrap flex-md-column-reverse d-md-flex">

    @include('layouts.nav')
    
    <div class="hero-wrap hero-wrap-2" style="background-image: url('{{asset('images/bg_2.jpg')}}');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end">
                <div class="col-md-9 ftco-animate pb-5">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{url('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Projects <i class="fa fa-chevron-right"></i></span></p>
                    <h1 class="mb-0 bread">Projects</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <h2 class="mb-4">Our Best Projects</h2>
            </div>
        </div>
         <div class="row no-gutters">
            <div class="col-md-6 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-1.jpg);">
                    <a href="images/work-1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/project_detail/towers')}}">Towers</a></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-7.jpg);">
                    <a href="images/work-7.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/project_detail/steel-shed')}}">Steel Structures/ Steel Sheds</a></h2>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-md-6 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/solar/1.jpg);">
                    <a href="images/solar/1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/project_detail/solar-mount')}}">Solar Mounting Systems</a></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/fabrication/1.jpg);">
                    <a href="images/fabrication/1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/project_detail/manu-products')}}">Manufacturing of Products</a></h2>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-md-6 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/mechanical/1.jpg);">
                    <a href="images/mechanical/1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/project_detail/mechanical-const')}}">Mechanical Construction</a></h2>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-md-6 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/design/1.jpg);">
                    <a href="images/design/1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/project_detail/design')}}">Design</a></h2>
                        </div>
                    </div>
                </div>
            </div>
            

        </div>
    </div>
</section>

@endsection

