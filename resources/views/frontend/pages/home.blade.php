@extends('layouts.app')
@section($active_tab,"active")
@section('title', ucWords($active_tab))

@section('content')

@section('css')

<style>
    .testimony-wrap .user-img {

        width: 120px;
        height: 120px;
        border-radius: 0%;

    }

</style>
@stop

@include('layouts.header')

<section class="menu-wrap flex-md-column-reverse d-md-flex">

    @include('layouts.nav')
    
    <!-- END nav -->
    <div class="hero-wrap js-fullheight">
        <div class="home-slider js-fullheight owl-carousel">
            <div class="slider-item js-fullheight" style="background-image:url(images/bg_1.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                        <div class="col-md-10 text-center ftco-animate">
                            <div class="text w-100">
                                <h2>WE ARE LEADING EPC CONTRACTORS</h2>
                                <h1 class="mb-4">DEDICATED IN PROVIDING QUALITY CONSTRUCTION WORLDWIDE.</h1>
                                <p><a href="{{url('/contact')}}" class="btn btn-primary">Book an appointment</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="slider-item js-fullheight" style="background-image:url(images/bg_2.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                        <div class="col-md-8 text-center ftco-animate">
                            <div class="text w-100">
                                <h2>We care about your home</h2>
                                <h1 class="mb-4">BEST SOLAR STRUCTURE MANUFACTURERS IN PAKISTAN</h1>
                                <p><a href="{{url('/contact')}}" class="btn btn-primary">Book an appointment</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-no-pt ftco-no-pb bg-light ftco-appointment">
    <div class="container">
        <div class="row">
            <div class="row justify-content-center">
                <div class="col-md-8 col-md-offset-2">
                    <div class="wrapper">
                        <div class="col-md-12 pl-md-5">
                            <div class="row no-gutters">
                                @include('includes.vision')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-counter" id="section-counter">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon"><span class="fa fa-calendar"></span></div>
                        <strong class="number" data-number="20">0</strong>
                        <span>Years of Experienced</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon"><span class="fa fa-briefcase"></span></div>
                        <strong class="number" data-number="1500">0</strong>
                        <span>Project completed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon"><span class="fa fa-users"></span></div>
                        <strong class="number" data-number="500">0</strong>
                        <span>Happy Clients</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                    <div class="text">
                        <div class="icon"><span class="fa fa-bar-chart"></span></div>
                        <strong class="number" data-number="10">0</strong>
                        <span>Business Partners</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <h2 class="mb-4">Our Featured Services</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="row tabulation mt-4 ftco-animate">
                    <div class="col-md-4">
                        <ul class="nav nav-pills nav-fill d-md-flex d-block flex-column">
                         @foreach(services() as $title => $service)
                         <li class="nav-item text-left">
                            <a class="nav-link {{$service['slug'] == 'services-1' ? 'active' : ''}} py-4" data-toggle="tab" href="#{{$service['slug']}}"><span class="{{$service['icon']}} mr-2"></span> {{$title}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-8 pl-md-4">
                    <div class="tab-content">

                       @foreach(services() as $title => $service)

                       <div class="tab-pane container p-0 {{$service['slug'] == 'services-1' ? 'active' : ''}}" id="{{$service['slug']}}">
                        <h3><a href="#">{{$title}}</a></h3>
                        <p>{{$service['paragraph']}}</p>
                        {!! $service['description'] !!}
                        <a href="{{ url('/service_detail/'.$service['slug']) }}">Detail <span class="fa fa-arrow-right"></span></a> 

                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 d-flex align-items-stretch">
        <div class="img" style="background-image: url(images/about.jpg);"></div>
    </div>
</div>
</div>
</section>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center pb-5 mb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading">Industry Applications</span>
                <h2>Industry Applications</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Power Generation</span>
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check mr-2"></span>Hydro Power Plants</li>
                            <li><span class="fa fa-check mr-2"></span>Oil and Gas Plants</li>
                            <li><span class="fa fa-check mr-2"></span>Coal Fired Boiler Plants</li>
                            <li><span class="fa fa-check mr-2"></span>Rice Husk Boiler Plants</li>
                            <li><span class="fa fa-check mr-2"></span>Wind Energy</li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Cement Plants</span>
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check mr-2"></span>Carrying out Civil Work Activities </li>
                            <li><span class="fa fa-check mr-2"></span>Carrying out Mechanical Activities</li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Transport</span>
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check mr-2"></span>Rapid Transit system</li>
                            <li><span class="fa fa-check mr-2"></span>Railways line Infrastructure</li>
                        </ul>

                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Automobile Industry</span>
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check mr-2"></span>Supply of Various parts for Tractors, Bikes and Cars </li>
                        </ul>

                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Telecom</span>
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check mr-2"></span>BTS Build outs</li>
                        </ul>

                    </div>
                </div>
            </div>


            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Hotels</span>
                        <ul class="pricing-text mb-2">
                            <li><span class="fa fa-check mr-2"></span>Construction of Hotels</li>
                        </ul>

                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Wedding Marquee</span>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-3 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">HVAC Air Conditioning </span>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-4 ftco-animate">
                <div class="block-7">
                    <div class="text-center">
                        <span class="excerpt d-block">Electrical and Plumbing works</span>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>

<section class="ftco-section ftco-no-pb ftco-no-pt bg-light">
    <div class="container">
        <div class="row justify-content-center pb-5 mb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading">Products</span>
                <h2>Products</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid px-md-0">
        <div class="row no-gutters">
            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-1.jpg);">
                    <a href="images/work-1.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/tele-towers')}}">Telecommunication Towers</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-2.jpg);">
                    <a href="images/work-2.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/tele-towers')}}">Telecommunication Towers</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-4.jpg);">
                    <a href="images/work-4.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/tans-towers')}}">Transmission Towers</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-6.jpg);">
                    <a href="images/work-6.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/tans-towers')}}">Transmission Towers</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-8.jpg);">
                    <a href="images/work-8.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/steel-shed')}}">Steel Structures/ Steel Sheds</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-9.jpg);">
                    <a href="images/work-9.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/steel-shed')}}">Steel Structures/ Steel Sheds</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-11.jpg);">
                    <a href="images/work-11.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/solar-mount')}}">Solar Mounting Systems</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 ftco-animate">
                <div class="work img d-flex align-items-end" style="background-image: url(images/work-12.jpg);">
                    <a href="images/work-12.jpg" class="icon image-popup d-flex justify-content-center align-items-center">
                        <span class="fa fa-expand"></span>
                    </a>
                    <div class="desc w-100 px-4">
                        <div class="text w-100 mb-3">
                            <h2><a href="{{url('/product_detail/solar-mount')}}">Solar Mounting Systems</a></h2>
                            <span>Product</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>	

<section class="ftco-section testimony-section bg-light">
    <div class="container">
        <div class="row justify-content-center pb-5 mb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading">Clients</span>
                <h2>Our Clients</h2>
            </div>
        </div>
        <div class="row ftco-animate">
            <div class="col-md-12">
                <div class="carousel-testimony owl-carousel ftco-owl">

                    @foreach(getClientImages() as $title => $image)
                    <div class="item">
                        <div class="testimony-wrap py-4" >
                            <div class="user-img " style="background-image: url(images/clients/{{$image['title']}})">
                            </div>
                            <div class="py-1">
                                <p class="name"> <a target="_blank" href="{{$image['url']}}"> {{ucwords(str_replace(['_','-'], ' ', $title))}} </a></p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
