@extends('layouts.app')
@section($active_tab,"active")
@section('title', ucWords($active_tab))

@section('content')

@include('layouts.header')
<section class="menu-wrap flex-md-column-reverse d-md-flex">

    @include('layouts.nav')
    
    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end">
                <div class="col-md-9 ftco-animate pb-5">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{url('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Services <i class="fa fa-chevron-right"></i></span></p>
                    <h1 class="mb-0 bread">Services</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center pb-5 mb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2>We offer Services</h2>
                <p>
                    Majeed Sons is a Pakistan Based Company, which can undertake the Construction under full scope of the Project. Complete Civil/Structural, Mechanical, Electrical & Instrumentation Installation are the Major Areas of Construction.
                Majeed Sons has experienced Project Management teams who can work delicately up-to the extent of Clients Satisfaction, Timely Completion along-with Safety & Quality Standards. </p>
            </div>
        </div>
        <div class="row">

            @foreach(services() as $title => $service)
            <div class="col-md-6 services ftco-animate">
                <div class="d-block d-flex">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="{{$service['icon']}}"></span>
                    </div>
                    <div class="media-body pl-3">
                        <h3 class="heading">{{$title}}</h3>
                        <p>{{$service['paragraph']}}</p>
                        <p><a href="{{ url('/service_detail/'.$service['slug']) }}" class="btn-custom">Read more</a></p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>



@endsection
