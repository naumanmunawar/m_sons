@extends('layouts.app')
@section($active_tab,"active")
@section('title', ucWords($active_tab))

@section('content')

@include('layouts.header')

<section class="menu-wrap flex-md-column-reverse d-md-flex">
    
    @include('layouts.nav')
    <!-- END nav -->
    <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end">
                <div class="col-md-9 ftco-animate pb-5">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{url('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Contact us <i class="fa fa-chevron-right"></i></span></p>
                    <h1 class="mb-0 bread">Contact us</h1>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- <section class="ftco-section ftco-no-pt ftco-no-pb bg-light">
    <div class="container">
        <div class="row d-flex no-gutters">
            <div class="col-md-6 pl-md-5">
                <div class="row justify-content-start py-1 pt-5">
                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                      
                        <h5 class="mb-4">Adul Majeed Saqib - CEO</h5>
                        <div class="text">
                            <p><span>Email:</span> <a href="mailto:info@msonseng.com">info@msonseng.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-md-5">
                <div class="row justify-content-start py-1 pt-5">
                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                      
                        <h5 class="mb-4">Adul Majeed Saqib - CEO</h5>
                        <div class="text">
                            <p><span>Email:</span> <a href="mailto:info@msonseng.com">info@msonseng.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-md-5">
                <div class="row justify-content-start py-1">
                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                      
                        <h5 class="mb-4">Adul Majeed Saqib - CEO</h5>
                        <div class="text">
                            <p><span>Email:</span> <a href="mailto:info@msonseng.com">info@msonseng.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-md-5">
                <div class="row justify-content-start py-1">
                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                      
                        <h5 class="mb-4">Adul Majeed Saqib - CEO</h5>
                        <div class="text">
                            <p><span>Email:</span> <a href="mailto:info@msonseng.com">info@msonseng.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="row no-gutters">
                        {{-- <div class="col-md-7 d-flex">
                            <div class="contact-wrap w-100 p-md-5 p-4">
                                <h3 class="mb-4">Get in touch</h3>
                                <form method="POST" id="contactForm" class="contactForm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6"> 
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea name="message" class="form-control" id="message" cols="30" rows="7" placeholder="Message"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="submit" value="Send Message" class="btn btn-primary">
                                                <div class="submitting"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> --}}

                        <div class="col-md-7">
                            <div class="col-md-12 pl-md-5">
                                <div class="row justify-content-start py-3">
                                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                                      
                                        <h5 class="mb-4">Adul Majeed Saqib - CEO</h5>
                                        <div class="text">
                                            <p><span>Email:</span> <a href="mailto:info@msonseng.com">info@msonseng.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 pl-md-5">
                                <div class="row justify-content-start py-1">
                                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                                      
                                        <h5 class="mb-4">Umair Majeed - Procurement Manager</h5>
                                        <div class="text">
                                            <p><span>Email:</span> <a href="mailto:umair@msonseng.com">umair@msonseng.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 pl-md-5">
                                <div class="row justify-content-start py-1">
                                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                                      
                                        <h5 class="mb-4">Abubakr Majeed - Ar. Engineer</h5>
                                        <div class="text">
                                            <p><span>Email:</span> <a href="mailto:abubakr@msonseng.com">abubakr@msonseng.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 pl-md-5">
                                <div class="row justify-content-start py-1">
                                    <div class="col-md-12 heading-section ftco-animate fadeInUp ftco-animated">
                                      
                                        <h5 class="mb-4">Asad Waqas - Mechanical Engineer </h5>
                                        <div class="text">
                                            <p><span>Email:</span> <a href="mailto:bdm@msonseng.com">bdm@msonseng.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 d-flex align-items-stretch">
                            <div class="info-wrap bg-primary w-100 p-lg-5 p-4">
                                <h3 class="mb-4 mt-md-4">Contact us</h3>
                                <div class="dbox w-100 d-flex align-items-start">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-map-marker"></span>
                                    </div>
                                    <div class="text pl-3">
                                        <p><span>Address:</span> Flat # 8, 1st Floor, 2 Civic Center,
                                            Moon Market, Faisal Town,
                                            Lahore, Pakistan.</p>
                                    </div>
                                </div>
                                <div class="dbox w-100 d-flex align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-phone"></span>
                                    </div>
                                    <div class="text pl-3">
                                        <p><span>Phone:</span> <a href="tel://+92-42-35184203">+92-42-35184203</a></p>
                                    </div>
                                </div>
                                <div class="dbox w-100 d-flex align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-paper-plane"></span>
                                    </div>
                                    <div class="text pl-3">
                                        <p><span>Email:</span> <a href="mailto:info@msonseng.com">info@msonseng.com</a></p>
                                    </div>
                                </div>
                                <div class="dbox w-100 d-flex align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-globe"></span>
                                    </div>
                                    <div class="text pl-3">
                                        <p><span>Website</span> <a href="http://www.msonseng.com">http://www.msonseng.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="map_show" class="map">
    <iframe scrolling="no" marginheight="0" marginwidth="0" id="gmap_canvas" src="https://maps.google.com/maps?&amp;hl=en&amp;q=Civic%20Center%20Commercial%20Area%20Faisal%20Town,%20Lahore%20Lahore+(Majeed%20Sons)&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed" width="100%" height="500" frameborder="0"></iframe> <a href='https://www.add-map.net/'>add google map</a> <script type='text/javascript' src='https://maps-generator.com/google-maps-authorization/script.js?id=fdcbae70f0258995a4323ce69906e85a7bdcca4a'></script>
</div>

@endsection
