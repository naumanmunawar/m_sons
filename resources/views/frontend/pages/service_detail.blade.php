@extends('layouts.app')
@section($active_tab,"active")
@section('title', ucWords($active_tab))

@section('content')

@section('css')

<style>
    .tabulation .tab-content ul li span.fa {
        font-size: 20px !important;
    }
</style>
@stop

@include('layouts.header')
<section class="menu-wrap flex-md-column-reverse d-md-flex">

    @include('layouts.nav')
    
    <div class="hero-wrap hero-wrap-2" style="background-image: url('{{asset('images/bg_2.jpg')}}');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end">
                <div class="col-md-9 ftco-animate pb-5">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="{{url('/')}}">Home <i class="fa fa-chevron-right"></i></a></span> <span>Services <i class="fa fa-chevron-right"></i></span></p>
                    <h1 class="mb-0 bread">Services</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8 text-center heading-section ftco-animate">
                <h2 class="mb-4">Our Best Services</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row tabulation mt-4 ftco-animate">
                    <div class="col-md-4">
                        <ul class="nav nav-pills nav-fill d-md-flex d-block flex-column">
                         @foreach(services() as $title => $service)
                         <li class="nav-item text-left">
                            <a class="nav-link {{$service['slug'] == 'services-1' ? 'active' : ''}} py-4" data-toggle="tab" href="#{{$service['slug']}}"><span class="{{$service['icon']}} mr-2"></span> {{$title}}</a>
                        </li>
                        @endforeach
                        
                    </ul>
                </div>
                <div class="col-md-8 pl-md-4">
                    <div class="tab-content">
                        <div class="tab-pane container p-0 active" id="services-1">
                            <h3><a href="#">Civil Construction</a></h3>
                            
                            <div class="md-input-container>
                                <label>Username</label>
                                <input type="text" ng-model="user.name">
                                
                                <div class="img" style="background-image: url({{asset('images/civil.png')}});"></div>
                            </div> 

                            <p>Majeed Sons have a capability, experience and equipment to undertake the execution of medium to large Civil Construction Projects. A Complete range of Expertise which covers the following areas</p>
                            <ul>
                                <li> <h5 class="h5"> <span class="fa fa-check"></span> Earthworks </h5>
                                    <ul>
                                        <li>Site Preparation </li>
                                        <li>Embankment Construction  </li>
                                        <li>Access and “Right of Way” (ROW) roads  </li>
                                    </ul>
                                </li>
                                <li> <h5 class="h5"> <span class="fa fa-check"></span> Foundations  </h5>
                                    <ul>
                                        <li>For Static Equipment </li>
                                        <li>For Rotary Equipment</li>
                                        <li>Rafts</li>
                                        <li>Isolated </li>
                                        <li>Piling</li>
                                    </ul>
                                </li>
                                <li> <h5 class="h5"> <span class="fa fa-check"></span> Structures </h5>

                                    <ul>
                                        <li>Steel Structure</li>
                                        <li>RCC Frame</li>
                                        <li>Precast Frame</li>
                                        <li>Masonry Structure </li> 
                                    </ul>
                                </li>

                                <li> <h5 class="h5"> <span class="fa fa-check"></span> Finishes </h5>

                                    <ul>
                                        <li>Flooring</li>
                                        <li>Roof treatment</li>
                                        <li>False ceiling</li>
                                        <li>Painting</li> 
                                        <li>Graphiauto Paint</li> 
                                        <li>Wood Work</li> 
                                        <li>Door and Window</li> 
                                        <li>Wooden Floor</li> 
                                        <li>Painting</li> 
                                        <li>Partition Wall</li> 
                                        <li>Paneling</li> 
                                        <li>Cup boards</li> 
                                        <li>Sanitary Fittings</li>  
                                    </ul>
                                </li>

                                <li> <h5 class="h5"> <span class="fa fa-check"></span> Aluminium Works </h5>

                                    <ul>
                                        <li>Doors and Window</li>
                                        <li>Curtain wall</li>
                                    </ul>
                                </li>

                            </ul>

                        </div>
                        <div class="tab-pane container p-0 fade" id="services-2">
                            <h3><a href="#">Mechanical Erection</a></h3>

                            <div class="md-input-container>
                                <label>Username</label>
                                <input type="text" ng-model="user.name">
                                
                                <div class="img" style="background-image: url({{asset('images/mechanical.jpg')}});"></div>
                            </div> 

                            <p>Majeed Sons provides Mechanical Erection Services for “grass-root” Projects as well as for expansion / revamping of existing facilities. It has considerable experience in the Erection of Equipment (Including Heavy Lifts) Piping, Pipe racks, Steel Structure, Tanks, Painting, Insulations, NDT and Commissioning. </p>
                            <ul>
                                <li><span class="fa fa-check"></span>Complete Plant Erection, Including Inland Transportation, Start up and Commissioning. </li>
                                <li><span class="fa fa-check"></span>Erection of Power Plant Boilers including Commissioning. </li>
                                <li><span class="fa fa-check"></span>Fabrication & Erection of Steel Structure.</li>
                                <li><span class="fa fa-check"></span>Fabrication & Erection of Piping / Pipe Racks.</li>
                                <li><span class="fa fa-check"></span>Fabrication & Erection of Tanks.</li>
                                <li><span class="fa fa-check"></span>Rigging / Heavy Lifts.</li>
                                <li><span class="fa fa-check"></span>Fabrication & Erection of Water Supply System.</li>
                                <li><span class="fa fa-check"></span>Cross Country Pipeline.</li>
                                <li><p> Majeed Sons owns a fleet of Mobile Cranes & other Equipment required to execute large Projects simultaneously. The Company periodically invests in Machinery/ Equipment, to increase and update its inventory of Facilitating Execution teams to ensure timely completion of the Projects.</p> </li>
                            </ul>
                        </div>
                        <div class="tab-pane container p-0 fade" id="services-3">
                            <h3><a href="#">Electrical & Instrumentation Installation</a></h3>

                            <div class="md-input-container>
                                <label>Username</label>
                                <input type="text" ng-model="user.name">
                                
                                <div class="img" style="background-image: url({{asset('images/electirical.jpg')}});"></div>
                            </div> 

                            <p>
                            Majeed Sons also provides services for Erection, Testing and Commissioning of Power, breitling replica watches Electrical & Instrumentation Jobs. </p>
                            <ul>
                                <li> <h5 class="h5"> <span class="fa fa-check"></span> Electrical </h5>

                                    <p>These Services includes Erection, Testing & Commissioning of:</p>
                                    <ul>
                                        <li>HV Power Transformers</li>
                                        <li>HV Switchgear & Switchyard Equipment</li>
                                        <li>HV Power Cables</li>
                                        <li>HV Transmission and Distribution Lines</li>
                                        <li>Turbo Generators and Diesel Generating sets</li>
                                        <li>Emergency Power Generators</li>
                                        <li>Telecommunication Systems</li>
                                        <li>Fire Detection & Alarm Systems</li>
                                        <li>Motor Control Centers</li>
                                        <li>Grounding Systems</li>
                                        <li>External & Internal Lighting</li>
                                        <li>Distribution Network</li>
                                        <li>Package Units</li>
                                        <li>Motors & Generators</li>
                                    </ul>
                                </li>
                                <li> <h5 class="h5"> <span class="fa fa-check"></span> Instrumentation </h5>

                                    <p>This area included Erection, Testing & Commissioning of Control Systems and field mounted Pneumatic Systems. Expertise is available for Installation of: </p>
                                    <ul>
                                        <li>Control Panel & Boards</li>
                                        <li>Distributed Control Systems</li>
                                        <li>In-Line Instruments</li>
                                        <li>Microprocessor based instruments</li>
                                        <li>Cabling and Wiring</li>
                                        <li>Pneumatic Tubing</li>
                                        <li>Cable Tray Work</li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                        <div class="tab-pane container p-0 fade" id="services-4">
                            <h3><a href="#">Fabrication & Manufacturing</a></h3>

                            <div class="md-input-container>
                                <label>Username</label>
                                <input type="text" ng-model="user.name">
                                <div class="img" style="background-image: url({{asset('images/fabrication.jpg')}});"></div>
                            </div> 

                            <p>Majeed Sons is a Pakistan Based Company, which can undertake the Manufacturing & Fabrication works. We Manufacture/ Fabricate Boiler, Pre-Heater, Pipe Supports Storage Tanks, Steel Structure, Steel Towers, Steel Structure Rib/ Arch, Storage Sheds, Waste Water Treatment Plant, Conveyor, Platforms, Cable Trays, etc.
                                Majeed Sons Manufacturing Works has experienced teams who work dedicatedly up-to the extent of Clients Satisfaction, timely completion along-with Safety and Quality Standards.
                                Majeed Sons workshop Located at Raiwind-Sundar Road, Breitling Replica Lahore is well equipped with Latest Machinery & Technically Strong works who could achieve the Milestone in Manufacturing/ Fabrication Works.
                                We have a fully equipped state of the art production facility operated by highly skilled and experienced worksforce. Not only have we chosen the best and latest high tech machines to provide superb quality for our customers, but we also have a quality consicious workforce and QC operations at every station.
                                The Manufacturing process is streamlined to ensure that by the end of the production cycle each job is complete and ready for delivery on time. 
                            Our Quality management systems is compliant is the requirements of ISO 9001. </p>
                            <ul>

                                <li><h5 class="h5"> <span class="fa fa-check"></span> Production Capability </h5>
                                    <p>The times described below are used as a general reference. Timelines depend on the size of the job and location. We Target to meet the Schedule of Client Requirement.
                                    We have capability of Production of 500 Ton Steel per Month. </p>
                                </li>

                                <li><h5 class="h5"> <span class="fa fa-check"></span> Strength in Delivery  </h5>
                                    <p>The times described below are used as a general reference. Timelines depend on the size of the job and location. We Target to meet the Schedule of Client Requirement.
                                    We have capability of Production of 500 Ton Steel per Month. </p>
                                </li>

                            </ul>
                        </div>
                        <div class="tab-pane container p-0 fade" id="services-5">
                            <h3><a href="#">Telecommunication Sector Services</a></h3>

                            <div class="md-input-container>
                                <label>Username</label>
                                <input type="text" ng-model="user.name">
                                <div class="img" style="background-image: url({{asset('images/telecommunication.jpg')}});"></div>
                            <p> Specializing in the logistical problems associated with communication structure design and construction in isolated and remote areas. With a full complement, Majeed Sons can quickly mobilize equipment and personnel from start to finish. Majeed Sons is your answer to quality material, experience and support.
                                In the past four Years, Majeed Sons has installed 200 plus BTS Sites of Self Supporting/ Monopole Towers throughout Pakistan. Majeed Sons has fabricated 1600 Towers during the year 2004-2007.
                                The Integrated Tower Solution targets 3 main areas of the project:

                                Design interface during pre-contract planning phase.
                                Logistical support during the contract implementation phase.
                                Technical support during the contract implementation phase.
                            </p>
                            <ul>

                                <li><h5 class="h5"> <span class="fa fa-check"></span> Design Interface Phase  </h5> 

                                    <p>First, we work with the customer during the design phase to agree on design parameters and produce economical and standardized modular concepts. Second is early approval of the design concepts, beginning the process of sourcing and production capacity and materials to be procured as per Client requirement. Third, we identify and agree on stock level for typical tower components. The last element of the Design Interface Phase is confirming with the customer the proposed rollout schedule. Currently Designing of Towers are done by following approved Consultants of Mobilink / Warid / Telenor.
                                    </p>
                                    <ul>
                                        <li> Engineering Design Bureau, Lahore - EDB</li>
                                        <li>  Exponent Engineers, Lahore - EE</li>
                                    </ul>

                                </li>

                                <li><h5 class="h5"> <span class="fa fa-check"></span>Logistical Support</h5> 
                                    <p>Employing experienced personnel that will provide logistical support to maintain the minimum stock requirements during implementation is the heart of the Integrated Tower Solution. Trained personnel are responsible for quality and quantity checks of all materials arriving from the factory. On a site by site basis, the logistical support team issues appropriate tower packages for installation.</p>
                                </li>

                                <li><h5 class="h5"> <span class="fa fa-check"></span>Technical Support</h5>
                                    <p> The times described below are used as a general reference. Timelines depend on the size of the job and location. We Target to meet the Schedule of Client Requirement.    
                                    </p>

                                    <ul>
                                        <li>Production: We have capability of Production of 500 Ton Steel per Month.</li>
                                        <li>Site Build Out: We have capability of Handing over 30 Sites per Month.</li>
                                    </ul>
                                </li>

                            </ul>
                        </div>

                    </div>

                    <div class="tab-pane container p-0" id="services-6">
                            <h3><a href="#">Architecture Sector Services</a></h3>
                            
                            <div class="md-input-container>
                                <label>Username</label>
                                <input type="text" ng-model="user.name">
                            </div> 

                            <p>We are a professional design and construction service provider with our peculiar managerial policies required to cater to the modern and futuristic dynamics of the construction world.
                                Haris&Co. being in the business for the last 25 years strongly believes that Architecture is the keynote for the construction industry. We realize the value of design, character and aesthetics and its implementation in the field. Along with our very creative Lead Architect Abubakr Majeed, Haris&Co. has always focused on making exceptional and unique designs for residential, commercial and industrial buildings.
                            Our design philosophy is about playing with the modern lines and useful space planning in the given parameters. We create space that will provide you with the opportunity to experience philosophical and natural elements in the realm of the built environment. We make sure that a space designed has a meaning to it, and adds value to the time and money spent on it</p>

                             <div class="img" style="background-image: url({{asset('images/architecture/1.jpeg')}});"></div>
                             <div class="img" style="background-image: url({{asset('images/architecture/2.jpg')}});"></div>
                             <div class="img" style="background-image: url({{asset('images/architecture/3.png')}});"></div>
                             <div class="img" style="background-image: url({{asset('images/architecture/4.jpg')}});"></div>
                             <div class="img" style="background-image: url({{asset('images/architecture/5.jpeg')}});"></div>
                             <div class="img" style="background-image: url({{asset('images/architecture/6.jpeg')}});"></div>
                             <div class="img" style="background-image: url({{asset('images/architecture/7.jpeg')}});"></div>
                             <div class="img" style="background-image: url({{asset('images/architecture/8.jpg')}});"></div>
                        </div>


                </div>
            </div>
        </div>

    </div>
</div>
</section>

@endsection

@section('scripts')

<script>
    $(document).ready(function() {

        $('a[href="#'+'{{$hash}}'+'"]')[0].click()

    });
</script>

@stop
