<?php


/* 
*** Helper Function
*/


function services() {

	$services  = array (

		'Civil Construction' => array (
			'paragraph' => 'Majeed Sons have a capability, experience and equipment to undertake the execution of medium to large Civil Construction Projects. A Complete range of Expertise which covers the following areas',
			'slug' => 'services-1',
			'icon' => 'flaticon-roof-2',
			'description' => '<ul>
			<li><span class="fa fa-check"></span>Earthworks </li>
			<li><span class="fa fa-check"></span>Foundations</li>
			<li><span class="fa fa-check"></span>Structures</li>
			<li><span class="fa fa-check"></span>Finishes</li>
			<li><span class="fa fa-check"></span>Aluminium Works</li>
			</ul>',
		),
		'Mechanical Erection' => array (
			'paragraph' => 'Majeed Sons provides Mechanical Erection Services for “grass-root” Projects as well as for expansion / revamping of existing facilities. It has considerable experience in the Erection of Equipment (Including Heavy Lifts) Piping, Pipe racks, Steel Structure, Tanks, Painting, Insulations, NDT and Commissioning.',
			'slug' => 'services-2',
			'icon' => 'flaticon-roof',
			'description' => ' <ul>
			<li><span class="fa fa-check"></span>Complete Plant Erection, Including Inland Transportation, Start up and Commissioning. </li>
			<li><span class="fa fa-check"></span>Erection of Power Plant Boilers including Commissioning. </li>
			<li><span class="fa fa-check"></span>Fabrication & Erection of Steel Structure.</li>
			<li><span class="fa fa-check"></span>Fabrication & Erection of Piping / Pipe Racks.</li>
			<li><span class="fa fa-check"></span>Fabrication & Erection of Tanks.</li>
			<li><span class="fa fa-check"></span>Rigging / Heavy Lifts.</li>
			<li><span class="fa fa-check"></span>Fabrication & Erection of Water Supply System.</li>
			<li><span class="fa fa-check"></span>Cross Country Pipeline.</li>
			</ul>'
		),
		'Electrical & Instrumentation Installation' => array (
			'paragraph' => 'Majeed Sons also provides services for Erection, Testing and Commissioning of Power, breitling replica watches Electrical & Instrumentation Jobs.',
			'slug' => 'services-3',
			'icon' => 'flaticon-roof-5',
			'description' => '<ul>
			<li><span class="fa fa-check"></span>Electrical </li>
			<li><span class="fa fa-check"></span>Instrumentation </li>
			</ul>'
		),
		'Fabrication & Manufacturing' => array (
			'paragraph' => 'Majeed Sons is a Pakistan Based Company, which can undertake the Manufacturing & Fabrication works. We Manufacture/ Fabricate Boiler, Pre-Heater, Pipe Supports Storage Tanks, Steel Structure, Steel Towers, Steel Structure Rib/ Arch, Storage Sheds, Waste Water Treatment Plant, Conveyor, Platforms, Cable Trays, etc.',
			'slug' => 'services-4',
			'icon' => 'flaticon-roof-1',
			'description' => '<ul>
			<li><span class="fa fa-check"></span>Production Capability</li>
			<li><span class="fa fa-check"></span>Strength in Delivery </li>
			</ul>'
		),
		'Telecommunication Sector Services' => array (
			'paragraph' => 'Specializing in the logistical problems associated with communication structure design and construction in isolated and remote areas. With a full complement, Majeed Sons can quickly mobilize equipment and personnel from start to finish. Majeed Sons is your answer to quality material, experience and support.
			In the past four Years, Majeed Sons has installed 200 plus BTS Sites of Self Supporting/ Monopole Towers throughout Pakistan. Majeed Sons has fabricated 1600 Towers during the year 2004-2007.',
			'slug' => 'services-5',
			'icon' => 'flaticon-roof-4',
			'description' => '<ul>
			<li><span class="fa fa-check"></span>Design Interface Phase </li>
			<li><span class="fa fa-check"></span>Logistical Support </li>
			<li><span class="fa fa-check"></span>Technical Support</li>
			</ul>'
		),
		'Architecture Sector Services' => array (
			'paragraph' => 'We are a professional design and construction service provider with our peculiar managerial policies required to cater to the modern and futuristic dynamics of the construction world.
			Haris&Co. being in the business for the last 25 years strongly believes that Architecture is the keynote for the construction industry. We realize the value of design, character and aesthetics and its implementation in the field. Along with our very creative Lead Architect Abubakr Majeed, Haris&Co. has always focused on making exceptional and unique designs for residential, commercial and industrial buildings.
			Our design philosophy is about playing with the modern lines and useful space planning in the given parameters. We create space that will provide you with the opportunity to experience philosophical and natural elements in the realm of the built environment. We make sure that a space designed has a meaning to it, and adds value to the time and money spent on it',
			'slug' => 'services-6',
			'icon' => 'flaticon-roof-2',
			'description' => ''
		)
	);

return $services;

}

function clientUrl() {

	$urls = array(
		'mobilink'	=> 'https://jazz.com.pk/',
		'telenor'	=> 'https://www.telenor.com.pk/',
		'warid'	=> 'https://jazz.com.pk/',
		'ufone'	=> 'https://www.ufone.com/',
		'ptcl'	=> 'www.ptcl.com.pk',
		'worldcall'	=> 'http://www.worldcall.com.pk/',
		'clough'	=> 'https://www.cloughgroup.com/',
		'siemens'	=> 'https://www.siemens.com/global/en.html',
		'ericsson'	=> 'https://www.ericsson.com/en',
		'ge_international_operations'	=> 'https://www.ge.com/',
		'pcl'	=> 'https://www.pcl.com/Pages/default.aspx',
		'presson_descon'	=> 'http://www.pdil.com/pdil/',
		'zte'	=> 'wwwen.zte.com.cn/en/',
		'huawei'	=> 'https://www.huawei.com/en/',
		'omv'	=> 'https://www.omv.com/en',
		'petronas'	=> 'https://www.petronas.com/',
		'maple_leaf'	=> 'http://www.kmlg.com/',
		'lafarge'	=> 'https://www.lafarge.ca/en',
		'pearl_continental'	=> 'https://www.pchotels.com/',
		'libirty'	=> '',
		'clough-safty'	=> '',
	);

	return $urls;
}


function getClientImages() {

	$path = 'images'.DIRECTORY_SEPARATOR.'/clients';
	$images =  File::allFiles($path);
	$image_urls =	clientUrl();

	foreach ($images as $title => $image) {

		$image_urls[pathinfo($image)['filename']] = array(
			'title' => pathinfo($image)['basename'],
			'url' => $image_urls[pathinfo($image)['filename']]
		);
	}

	return $image_urls;
}
