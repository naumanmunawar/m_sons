<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

        $path = 'frontend';
        $this->path = $path;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data['active_tab'] = 'home';
        return view($this->path.'.pages.home', $data);
    }
    
    public function about()
    {
        $data['active_tab'] = 'about';
        return view($this->path.'.pages.about', $data);
    }
    
    public function services()
    {
        $data['active_tab'] = 'services';
        return view($this->path.'.pages.services', $data);
    }

    public function service_detail($hash)
    {
        $data['active_tab'] = 'services';
        $data['hash'] = $hash;
        return view($this->path.'.pages.service_detail', $data);
    }

    public function projects()
    {
        $data['active_tab'] = 'projects';
        return view($this->path.'.pages.projects', $data);
    }
    
    public function project_detail($hash)
    {
        $data['active_tab'] = 'projects';
        $data['hash'] = $hash;
        return view($this->path.'.pages.project_detail', $data);
    }

    public function clients()
    {
        $data['active_tab'] = 'clients';
        return view($this->path.'.pages.clients', $data);
    }

     public function sustainability()
    {
        $data['active_tab'] = 'sustainability';
        return view($this->path.'.pages.sustainability', $data);
    }

    public function careers()
    {
        $data['active_tab'] = 'careers';
        return view($this->path.'.pages.careers', $data);
    }


    public function contact()
    {
        $data['active_tab'] = 'contact';
        return view($this->path.'.pages.contact', $data);
    }

}
